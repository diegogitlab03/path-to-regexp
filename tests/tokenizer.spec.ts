import { test } from '@japa/runner';
import tokenizer from '../src/tokenizer';

test.group('tokenizer', () => {
	test('It should generate tokens for a simple path', ({ expect }) => {
		const tokens = tokenizer('/users');

		expect(tokens).toMatchObject([{ type: 'name', value: 'users' }]);
	});

	test('It should generate tokens for a path with params', ({ expect }) => {
		const tokens = tokenizer('/:id');

		expect(tokens).toMatchObject([{ type: 'param', value: 'id' }]);
	});

	test('It should generate tokens for a simple path with params', ({ expect }) => {
		const tokens = tokenizer('/users/:id');

		expect(tokens).toMatchObject([{ type: 'name', value: 'users' }, { type: 'param', value: 'id' }]);
	});

	test('It should throw a error when path is invalid', ({ expect }) => {
		expect(() => tokenizer('/%invalid%')).toThrowError();
	});
});
