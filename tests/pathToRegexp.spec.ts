import { test } from '@japa/runner';
import { pathToRegexp } from '../src';

test.group('pathToRegexp', () => {
	test('It should create a regex for a simple path', ({ expect }) => {
		const regex = pathToRegexp('/users');

		expect('/users'.match(regex)).not.toBe(null);
	});

	test('It should create a regex for simple path that recuses invalid urls', ({ expect }) => {
		const regex = pathToRegexp('/users');

		expect('/user'.match(regex)).toBe(null);
		expect('/users//'.match(regex)).toBe(null);
		expect('//users/'.match(regex)).toBe(null);
		expect('users'.match(regex)).toBe(null);
	});

	test('It should create a regex for a path with params', ({ expect }) => {
		const regex = pathToRegexp('/users/:id');

		expect('/users/1'.match(regex)).not.toBe(null);
	});

	test('It should create a regex for a path with multiple params', ({ expect }) => {
		const regex = pathToRegexp('/users/:blog_id/posts/:post_id');

		expect('/users/1/posts/2'.match(regex)).not.toBe(null);
	});

	test('It should create a regex for a path with optional param', ({ expect }) => {
		const regex = pathToRegexp('/users/:id?');

		expect('/users/1'.match(regex)).not.toBeNull();
		expect('/users'.match(regex)).not.toBeNull();
	});
});
