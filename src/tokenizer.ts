const PARAM_REGEX = /^:\w+$/;
const OPTIONAL_PARAM_REGEX = /^:\w+\?$/;
const TEXT_REGEX = /^\w+$/;

interface Token {
    type: string;
    value: string;
}

function tokenizer (input: string): Token[] {
	const parts = input.split('/');
	const tokens: Token[] = [];

	if (parts[0] === '') {
		parts.shift();
	}

	parts.forEach(part => {
		if (PARAM_REGEX.test(part)) {
			const paramAlias = part.split('');
			paramAlias.shift();

			tokens.push({
				type: 'param',
				value: paramAlias.join('')
			});

			return;
		} else if (OPTIONAL_PARAM_REGEX.test(part)) {
			const paramAlias = part.split('');
			paramAlias.shift();
			paramAlias.pop();

			tokens.push({
				type: 'optional_param',
				value: paramAlias.join('')
			});
		} else if (TEXT_REGEX.test(part)) {
			tokens.push({
				type: 'name',
				value: part
			});

			return;
		} else {
			throw new Error('Invalid route');
		}
	});

	return tokens;
}

export default tokenizer;
