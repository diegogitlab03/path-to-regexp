import tokenizer from './tokenizer';

export function pathToRegexp (path: string): RegExp {
	const tokens = tokenizer(path);
	let regexString = '';

	tokens.forEach(token => {
		if (token.type === 'param') {
			regexString += `/(?<${token.value}>\\w+)`;
		} else if (token.type === 'optional_param') {
			regexString += `(/(?<${token.value}>\\w+))?`;
		} else if (token.type === 'name') {
			regexString += `/${token.value}`;
		}
	});

	regexString = `^${regexString}$`;

	return new RegExp(regexString);
}
