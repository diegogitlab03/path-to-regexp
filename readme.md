# Path to regexp

A simple path to regexp library

## Example

```js
const { pathToRegexp } = require('dh-path-to-regex');

const regexUsers = pathToRegexp('/users');

console.log(regexUsers);
console.log('/users'.match(regexUsers));
console.log('/users/'.match(regexUsers));

/**
 * With Params
 */
const regexUserById = pathToRegexp('/users/:id');

console.log(regexUserById);
console.log('/users/1'.match(regexUserById));
console.log('/users/1/'.match(regexUserById));

const regexTasksByUser = pathToRegexp('/users/:user_id/tasks');

console.log(regexTasksByUser);
console.log('/users/1/tasks'.match(regexTasksByUser));

const regexTasksByIdAndUser = pathToRegexp('/users/:user_id/tasks/:task_id');

console.log(regexTasksByIdAndUser);
console.log('/users/2/tasks/1'.match(regexTasksByIdAndUser));

/**
 * Optional params
 */
const regexUserShowOrList = pathToRegexp('/users/:id?');

console.log(regexUserShowOrList.match('/users/1'));
console.log(regexUserShowOrList.match('/users'));
```
